/**
 * Appcelerator Titanium Mobile
 * Copyright (c) 2009-2014 by Appcelerator, Inc. All Rights Reserved.
 * Licensed under the terms of the Apache Public License
 * Please see the LICENSE included with this distribution for details.
 *
 * WARNING: This is generated code. Do not modify. Your changes *will* be lost.
 */

#import "ApplicationMods.h"

@implementation ApplicationMods

+ (NSArray*) compiledMods
{
	NSMutableArray *modules = [NSMutableArray array];

	
		[modules addObject:[NSDictionary
			dictionaryWithObjectsAndKeys:@"draggable",
			@"name",
			@"ti.draggable",
			@"moduleid",
			@"1.3.1",
			@"version",
			@"e8c13998-8fa8-4cee-8078-353c27e84d19",
			@"guid",
			@"",
			@"licensekey",
			nil
		]];
		

	return modules;
}

@end