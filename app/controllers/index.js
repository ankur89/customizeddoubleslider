var win = $.wind;
var TiDraggable = require('ti.draggable');
var labelLeft = Ti.UI.createLabel({
	text:"",
	font:{fontSize:12},
	color:"black"
});
var labelRight = Ti.UI.createLabel({
	text:"",
	font:{fontSize:12},
	color:"black"
});
var knobRight = TiDraggable.createView({
    center: { x: 30, y: 105 },
    width: 40, height: 40,
    minLeft: 20,
    maxLeft: 200,
    backgroundImage:'/button.png',
    axis: 'x',
    knob: 'right'
});
 
var knobLeft = TiDraggable.createView({
    center: { x: 30, y: 105 },
    width: 40, height: 40,
    minLeft: 20,
    maxLeft: 200,
    backgroundImage:'/button.png',
    axis: 'x',
    knob: 'left'
});
 
var barView = Ti.UI.createView({
    left: 30,
    right: 30,
    height: 10,
    top: 100,
    borderColor: 'black',
    borderWidth: 1
});
 
var fillView = Ti.UI.createView({
    left: 31,
    // right: 31,
    height: 8,
    top: 101,
    backgroundColor: 'green'
});
 
knobLeft.add(labelLeft);
knobRight.add(labelRight);
win.add(barView);
win.add(fillView);
win.add(knobRight);
win.add(knobLeft);
 
function barViewPostLayout(_event) {
    barView.removeEventListener('postlayout', barViewPostLayout);
 
    var rect = _event.source.rect;
    var x = rect.x + rect.width;
    knobRight.center = { x: x, y: 105} ;
    knobRight.maxLeft = x - (knobRight.width / 2);
    
    fillView.left = knobLeft.center.x;
    fillView.width = knobRight.center.x - knobLeft.center.x;
}
 
function onKnobMove(e) {
    e.source.center = e.center;
    var barRect ={};
    barRect = barView.rect;
    
    knobRight.minLeft = knobLeft.center.x + (knobLeft.width / 2);
    knobLeft.maxLeft = knobRight.center.x - knobRight.width - (knobRight.width / 2);
    
    fillView.left = knobLeft.center.x;
    fillView.width = knobRight.center.x - knobLeft.center.x;
    var val=(e.source.rect.x + (e.source.width / 2) - barRect.x) / barRect.width;
    Ti.API.info("Value : ----->"+Math.round(val * 1000));
    var child = e.source.getChildren();
    child[0].text = Math.round(val * 1000);
}
 
knobLeft.addEventListener('move', onKnobMove);
knobRight.addEventListener('move', onKnobMove);
 
barView.addEventListener('postlayout', barViewPostLayout);
win.open();
