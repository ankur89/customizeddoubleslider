var TiDraggable = require('ti.draggable');
var ASSETS = '/slider';
var KNOB_IMAGE = 'button.png';
var BAR_IMAGE = 'slider.png';
 
function Slider(_params) {
    
    _params.knob = _params.knob || {};
    var knobImage = _params.knob.image || KNOB_IMAGE;
    var knobHeight = _params.knob.height || 57;
    var knobWidth = _params.knob.width || 53;
    
    _params.bar = _params.bar || {};
    _params.bar.backgroundImage = _params.bar.image || BAR_IMAGE;
    var callback = _params.callback || function(_e) {};
    
    var knob = TiDraggable.createView({
        width: knobWidth,
        height: knobHeight,
        backgroundImage: ASSETS + '/' + _params.knob.image,
        axis: 'x'
    });
    
    delete _params.knob;
    delete _params.callback;
    
    var barView = Ti.UI.createView(_params.bar);
    var barRect = {};
    function barViewPostLayout(_event) {
        barView.removeEventListener('postlayout', barViewPostLayout);
    
        barRect = barView.rect;
        knob.center = { x: barRect.x, y: barView.y - (knobHeight / 2)};
        knob.maxLeft = barRect.x + barRect.width - (knobWidth / 2);
        knob.minLeft = barRect.x - (knobWidth / 2);
        barView.parent.add(knob);
    }
    
    function onKnobMove(e) {
        callback((e.source.rect.x + (knobWidth / 2) - barRect.x) / barRect.width);
    }
    
    knob.addEventListener('move', onKnobMove);
    
    barView.addEventListener('postlayout', barViewPostLayout);
    
    this.view = barView;    
}
module.exports = Slider;